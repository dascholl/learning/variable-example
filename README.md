# GitLab CI Variable Build Example

Demonstrates a simple GitLab CI YAML build file.

The following variables have to be set for this pipeline to function properly.

### Project Level Variables

- VARIABLE1
- VARIABLE2

### Group Level Variables

- GROUPVAR
